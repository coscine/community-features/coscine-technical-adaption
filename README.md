# Welcome to the Coscine Technical Adaption Project

This project is dedicated to everything involving the techical adaption of Coscine. The working group behind this project is composed of various members of the [RWTH Aachen University](https://www.rwth-aachen.de/)'s [IT Center](https://www.itc.rwth-aachen.de/) working in the [Research Process and Data Management (RPDM) department](https://www.itc.rwth-aachen.de/cms/IT-Center/IT-Center/Profil/Organisation/~esvd/Abteilungen/). Our goal is to bundle our experiences and knowledge in creating technical solutions to interacting with Coscine and further these adapatations by engaging with the research community.

## Existing Tools

You can find existing tools such as scripts and programs listed below.

> ⚠️ Deprecation Warning: Tools and scripts may not have been updated for Coscine APIv2!

### Scripts


- [**Metadata to Coscine Examples**](https://git.rwth-aachen.de/coscine/community-features/metadata-to-coscine-examples/) is a collection of generic and real world examples in which (meta)data are transferred from a local computer or server to a resource in Coscine using a Python script. This also includes **metadata parsers** for certain data types.
- [**Excel Spreadsheet to Application Profile**](https://git.rwth-aachen.de/rdm-bundle/tools-and-scripts/-/tree/main/spreadsheetToAP) is a Jupyter Notebook that helps you generate the backbone of an application profile from an Excel spreadsheet and provides instrcutions on how to complete and submit it via the [Application Profile Generator](https://docs.coscine.de/de/rdm/metadata/application_profile_generator/about/). This is especially helpful for large application profiles.
- [**Custom Resource Frontend**](https://git.rwth-aachen.de/coscine/community-features/coscine-custom-resource-frontend) demonstrates the capabilities of the Static Site Generator via a sample project/CI Script and provides a guide for setting the generator up for your own Coscine resources with GitHub/GitLab.

### Community Projects

- [**Coscine Python SDK**](https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk) is an open source python package providing
a pythonic client interface to the Coscine REST API. It is compatible with Python versions 3.7+. [Documentation](https://coscine.pages.rwth-aachen.de/community-features/coscine-python-sdk/coscine/) is available on GitLab.
- [**Coscine Static Site Generator**](https://git.rwth-aachen.de/coscine/community-features/coscine-static-site-generator) 
    - generates a static website for Coscine resources
    - users can then host the resulting website on free static site hosting services like GitHub (pages) or GitLab (pages)
    - Makes data in Coscine accessible to third parties
    - Provides a SPARQL interface for metadata access
    - Generates a nice looking and customizable landing page for your research data for free
- **Repository Manager 2.0** is a dotnet tool consisting of multiple helpful scripts to manage a Coscine repository. The scripts include:
    - Bulk cloning of a remote repository or branch switching on a local repository
    - Preparing the release for the current Coscine team workflow
    - Generating the integrations file for deploying the latest available software versions 
    - `dev` branch creation on the remote repository
    - Rebasing of the `dev` branch to be up-to-date with the `master`/`main` branch
    - Merge rules definition for Merge Requests on the Coscine repository
    - Consul store configuration

### Stale projects (outdated/unmaintained)

These projects are not actively maintained anymore. Contact the authors if you would like to pick up development again and get them up to date.

- [**Coscine Upload GUI**](https://github.com/niklassiemer/CoScInE_Upload_GUI) is a graphical user interface based on tkinter for the coscine python package. Right now, only the upload of files to already initialized Resources is possible.
- [**Coscine Explorer**](https://git.rwth-aachen.de/coscine/community-features/coscine-explorer) is a basic GUI based on Python SDK as a showcase on how to do GUI bindings to Coscine
    - Currently set to private
    - Original purpose: Have a dedicated S3 client for people using S3 resources via Coscine with integrated Metadata management
    - Original purpose: Offer file previews like image viewing in app and editing e.g. csv in app
    - Currently not under active development, but might be picked up again in the future

## Coscine Documentation and Tutorials

- [API Examples in Coscine Documentation](https://git.rwth-aachen.de/coscine/docs/public/documentation)
    - [New Deployed Live API Docs](https://coscine.pages.rwth-aachen.de/docs/public/documentation/de/advanced/api/)
    - [New Deployed Live API python setup](https://coscine.pages.rwth-aachen.de/docs/public/documentation/de/advanced/python/)
<!-- removed application profile stuff, maybe rework and put back later, but official Docs really are fine  -->

## Contact Us

If you have questions regarding techical adaptations or would like to get involved and share your tools and solutions, [write us an e-mail](mailto:coscine-technical-adaptation@itc.rwth-aachen.de).

Please note: if you are having technical issues with Coscine or have general questions regarding the platform, please contact the [IT-ServiceDesk](https://help.itc.rwth-aachen.de/en/service/b734502cd73e4201b1f763a65a61bf9c/) and refer to the [official documentation](https://docs.coscine.de/de/).
