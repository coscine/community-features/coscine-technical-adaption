RWTH Aachen University

Templergraben 55

52062 Aachen (Anschrift)

52056 Aachen (Postanschrift)

Tel: +49 241 80 1

Fax: +49 241 80 92312

E-Mail: [servicedesk@itc.rwth-aachen.de](mailto:servicedesk@itc.rwth-aachen.de)

Die RWTH Aachen University ist eine Körperschaft des öffentlichen Rechts. Sie wird durch den Rektor, Univ.-Prof. Dr. rer. nat. Dr. h.c. mult. Ulrich Rüdiger, vertreten.

For more information, see [IT Center Imprint](https://www.itc.rwth-aachen.de/cms/IT-Center/Footer/Service/~epvv/Impressum/).

The Coscine Technical Adaption team is responsible for content, please contact: [coscine-technical-adaptation@itc.rwth-aachen.de](mailto:coscine-technical-adaptation@itc.rwth-aachen.de) 
