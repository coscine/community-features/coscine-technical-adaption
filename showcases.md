# Showcases

Here, we showcase user solution and implementations built around Coscine. The content will be continiously updated.

Showcases:
  * [HiBC: The Human Intestinal Bacterial Collection](showcase_hibc.md)

If you have something you would like to showcase, [get in touch via e-mail](mailto:coscine-technical-adaptation@itc.rwth-aachen.de) or create a merge request or issue in our [GitLab Project](https://git.rwth-aachen.de/coscine/community-features/coscine-technical-adaption).